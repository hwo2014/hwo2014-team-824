# -*- coding: utf-8 -*-
require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]
track_name = "usa"

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class Logger
  attr_writer :race_info

  def initialize()
    @log = []
  end

  def push(tick, l)
    @log[tick] = if (@log[tick])
                   @log[tick].merge(l)
                 else
                   l.merge({:gameTick => tick})
                 end
  end

  def save_log (filename)
    File.open("log/#{filename}", 'w') do |f|
      f.puts JSON.generate({:race => @race_info, :log => @log})
    end
  end
end

class Race
  class Track
    attr_accessor :name, :piece_info
    
    def initialize(track_info)
      @name       = track_info['name']
      @piece_info = track_info['pieces']
      @lane_info  = []
      for lane in track_info['lanes'] do
        @lane_info[lane['index']] = lane
      end
      @starting_point = track_info['starting_point']
    end
    
    def piece_length(piece_idx, lane_idx)
      piece = @piece_info[piece_idx]

      piece['length'] ||
      begin
        dist = @lane_info[lane_idx]['distanceFromCenter']
        angle = piece['angle']
        radius =  piece['radius']
        if angle > 0
          angle * (radius - dist) * Math::PI / 180.0
        else
          - angle * (radius + dist ) * Math::PI / 180.0
        end
      end
    end
    
    def piece_curve_p (piece_idx)
      @piece_info[piece_idx]['angle']
    end
  end

  attr_accessor :track

  def initialize(data)
    @rawdata = data
    
    @track = Track.new(data['track'])
  end
end





class Simulate
  def initialize(c1,c2,c3,c4)
    @c1 = c1
    @c2 = c2
    @c3 = c3
    @c4 = c4
  end
  def calc_dd_angle(speed, angle, rad)
    return [0, ((speed**2) / rad) * @c3 - @c4].max+( -angle[0] * @c2  - (angle[1] - angle[0]) * @c1)
  end
  def calc_d_angle(speed, d_speed, rad, angle)
    return angle[2] - angle[1] + calc_dd_angle(speed, angle, rad)
  end
  
  def calc_angle(speed, d_speed, rad, angle)
    return angle[2] + calc_d_angle(speed, d_speed, rad, angle)
  end
  
  def calc_d_speed(speed, throttle)
    return 0.02 * (throttle * 10 - speed)
  end
  
  def calc_speed(speed, throttle)
    return speed + calc_d_speed(speed, throttle)
  end

  
  def integrate_angle(init_speed, init_angle, limit_speed, limit_angle, throttle, piece)
    curve_angle = piece['angle']
    rad = piece['radius'].nil? ? Float::INFINITY : piece['radius']
    len = piece['length'].nil? ? (rad * Math::PI * curve_angle /180.0) : piece['length']   
    n_dist = 0
    time = 0
		n_speed = init_speed		
		n_angle = init_angle
		flg = true
    
		while n_dist < len
			n_dist = n_dist + n_speed
      #p n_dist
      n_speed= calc_speed(n_speed, throttle)
      n_angle= [n_angle[1], n_angle[2], calc_angle(n_speed, calc_d_speed(n_speed, throttle), rad, n_angle)]
			if n_angle[2] >= limit_angle || n_speed >= limit_speed
				flg = false
				break			
			end
      time += 1
		end
    if !flg
      time = Float::INFINITY
    end
    # p [init_speed, init_angle, limit_speed, limit_angle, throttle, time]
		return {:time => time, :speed => n_speed, :angle => n_angle[2]}
  end
	def limit_throttle(init_speed, init_angle, end_speed, end_angle, piece)
		low  = 0.1
		high = 1.0
		maxThrottle = 0.0	
		med = (high - low)/2.0			
    time = Float::INFINITY
    good = nil
    while high - low > 1e-6
      result = integrate_angle(init_speed, init_angle, end_angle, end_speed, med, piece)
      if result[:time] != Float::INFINITY
        good = result
				maxThrottle = med
        time = result[:time]
				low = med
			else
				high= med
			end
			med = (high + low)/2.0
		end
    p "good is"
    p good
		return {:time => time, :throttle => maxThrottle}
	end

  def calc_ideal_throttle(rlist)
    piece_list  = rlist.reverse + rlist.reverse + rlist.reverse
    angle_list = [0.0, 10.0, 20.0, 30.0, 40.0, 50.0]
    speed_list = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
    datalist = [[PredictData.new(60.0, Float::INFINITY, 0.0, 0.0, 1.0)]]
    for i in 0...piece_list.size
      tlist = []
      for data in datalist[datalist.size-1]
        for angle in angle_list
          for speed in speed_list
            t = limit_throttle(speed, angle, data.speed, data.angle, piece_list[i])
            if t[:time] != Float::INFINITY
              tlist.push(PredictData.new(angle, speed, t[:time], data.acc_time + t[:time] ,t[:throttle]))
            end
          end
        end
      end
      #p tlist
      datalist.push(tlist)
    end
    return datalist.reverse
  end
end
class PredictData
  attr_accessor :angle, :speed, :time, :acc_time, :throttle
  def initialize(angle, speed, time, acc_time, throttle)
    @angle = angle
    @speed = speed
    @time  = time
    @acc_time = acc_time
    @throttle = throttle
    @flg = 0
  end
  def <(other)
    return @acc_time < other.acc_time
  end
end

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key, track_name)
    # external state
    @logger = Logger.new
    @race   = nil
    @predict = nil
    # internal state
    @bot_name = bot_name
    @time = 0
    
    @prev_status = {
      :speed      => 0,
      :dist       => 0,
      :dist_piece => 0,
      :angle  => 0,
      :d_angle => 0,
      :lap        => 0,
      :piece      => 0,
      :lane       => 0,
      :throttle   => 0
    }
    

    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, track_name, tcp)
  end

  def play(bot_name, bot_key, track_name, tcp)
    tcp.puts join_message(bot_name, bot_key, track_name)
    react_to_messages_from_server tcp
  end
  
  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      @time = ((message['gameTick'])||0)
      #puts @time

      case msgType
      when 'gameInit'
        
        #sim  = Simulate.new()
        #@data = sim.calc_ideal_throttle(msgData['race']['track']['pieces'])
        @logger.race_info = msgData['race']
        @race = Race.new(msgData['race'])
        puts "Track name is #{@race.track.name}"
      when 'carPositions'
        my_data = nil
        for i in 0...msgData.length
          if @bot_name == msgData[i]['id']['name']
            my_data = msgData[i]
            break
          end
        end
        if (my_data)
          tcp.puts AI(my_data)
        end
      else
        case msgType
        when 'join'
          puts 'Joined'
        when 'gameStart'
          puts 'Race started'
        when 'crash'
          puts 'Someone crashed'
          if (msgData['name'] == @bot_name)
            @logger.push(@time, {:crash => true})
          end
        when 'spawn'
          if (msgData['name'] == @bot_name)
            @logger.push(@time, {:spawn => true})
          end
        when 'gameEnd'
          puts 'Race ended'
        when 'error'
          puts "ERROR: #{msgData}"
        end
        puts "Got #{msgType}"
        tcp.puts ping_message
      end
    end
    @logger.save_log("runlog_#{Time.now.strftime("%m_%d_%H_%M")}.json")
  end

  def join_message(bot_name, bot_key, track_name)
    make_msg("joinRace",
             {
               :botId => {:name => bot_name, :key => bot_key},
               :trackName => track_name,
               :carCount => 1
             })
  end
 
  def throttle_message(throttle)
    if(throttle >= 0)
      make_msg("throttle", [1.0, throttle].min)
    else
      make_msg("throttle", 0.0)
    end
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def get_piece_length(piece)
    #レーンを考えていないのがよくない
    len = piece['length']
    if len.nil?
      len = piece['angle'].abs * piece['radius'] * Math.atan(1.0)*4.mis0 / 180.0
    end
    return len
  end

  def fun_p(x)
    if x <= 0.0
      1.0
    else
      0.5 ** x
    end
  end
  

  def AI(my_data)
    lap       = my_data['piecePosition']['lap']
    piece_idx = my_data['piecePosition']['pieceIndex']
    lane_idx  = my_data['piecePosition']['lane']['startLaneIndex']
    angle     = my_data['angle']
    d_angle = angle - @prev_status[:angle]
    d2_angle= d_angle - @prev_status[:d_angle]
    dist_in_piece = my_data['piecePosition']['inPieceDistance']
    dist_piece    = @prev_status[:dist_piece]
    if (@prev_status[:piece] != piece_idx)
      dist_piece += @race.track.piece_length(@prev_status[:piece], lane_idx)
    end
    dist = dist_piece + dist_in_piece
    
    speed = dist - @prev_status[:dist]

    
    throttle = @prev_status[:throttle]
    if @flg == 1
      @predict = Simulate.new(-0.0117,-0.1117,3.7565,1).limit_throttle(speed,
                                               [@prev_status[:angle], angle, angle],
                                               7.0,
                                               50,
                                               @race.track.piece_info[piece_idx])
      @limit_throttle = @predict[:throttle]
      p @limit_throttle
      @flg =0
    end

    if piece_idx != @prev_status[:piece]
      @flg = 1
    end
    
    
    #p my_data
    
    
    if (@prev_status[:piece] != piece_idx &&
        @race.track.piece_curve_p(@prev_status[:piece]) &&
        !@race.track.piece_curve_p(piece_idx))
      throttle = 0
    end

    if throttle == 0 && speed < 5
      throttle = 1
    end
    if !@limit_throttle.nil?
      throttle = @limit_throttle
    end
    tick_info = {
      :speed      => speed,
      :dist       => dist,
      :dist_piece => dist_piece,
      :angle      => angle,
      :d_angle    => d_angle,
      :lap        => lap,
      :piece      => piece_idx,
      :lane       => lane_idx,
      :throttle   => throttle
    }
    p [@time, speed, dist, angle, throttle].join " "
    @prev_status = tick_info
    @logger.push(@time, tick_info)
    n_piece = @race.track.piece_info[piece_idx]
    
    if @race.track.piece_info[piece_idx]['length'].nil?
      r=@race.track.piece_info[piece_idx]['radius']
      #print [@time, r, dist, speed, d_speed, angle, d_angle, d2_angle].join(" ") + "\n"
    else
      r=10000000000000.0
      #print [@time, r, dist, speed, d_speed, angle, d_angle, d2_angle].join(" ") + "\n"
    end
    throttle_message(throttle)
  end
  
end

NoobBot.new(server_host, server_port, bot_name, bot_key, track_name)
