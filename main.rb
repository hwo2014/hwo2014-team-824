# -*- coding: utf-8 -*-
require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"


class RunLog
  
  public
  
  def initialize(time, dist, speed, car_angle, lane, piece)
    @time = time
    @dist = dist
    @speed = speed
    @car_angle = car_angle
    @lane = lane
    @piece = piece
  end
  
  def <(other)
    return @time < other.time
  end
end

class LogList

  public
  
  def initialize()
    @logs=[RunLog.new(0,0,0,0,0,nil)]
  end
  
  def add_log(new_run_log)
    p new_run_log
    @logs.push(new_run_log)
  end
  
  def search_by_dist(distance)
    @logs.bsearch{|x| x.dist > distance} 
  end  
end
class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    @bot_name = bot_name
    @log_list = LogList.new()
    @map_name = "germany"
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end
  
  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType 
      when 'gameInit'
        @initial_data = msgData
        @acc_dist = []
        @v0_limit = []
        pieces = msgData['race']['track']['pieces']
        a = 0.0
        for i in 0...pieces.size
          len = get_piece_length(pieces[i])
          a += len
          @acc_dist[i] = a
        end
        for i in 0...pieces.size
          if pieces[i]['angle'].nil?
            @v0_limit[i] = Float::INFINITY
          else
            @v0_limit[i] = 0.7
          end
        end
        @lap = 0
        @dist = 0
        @speed = 0
        @time = 0
      when 'carPositions'
        for i in 0...msgData.length
          if @bot_name == msgData[i]['id']['name']
            my_data = msgData[i]
            break
          end
        end
        tcp.puts AI(my_data)
      else
        case msgType
        when 'join'
          puts 'Joined'
        when 'gameStart'
          puts 'Race started'
        when 'crash'
          puts 'Someone crashed'
        when 'gameEnd'
          puts 'Race ended'
        when 'error'
          puts "ERROR: #{msgData}"
        end
        puts "Got #{msgType}"
        tcp.puts ping_message
      end
    end
  end
  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end
  def throttle_message(throttle)
    if(throttle >= 0)
        make_msg("throttle", [1.0, throttle].min)
    else
      make_msg("throttle", 0.0)
    end
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
  def get_piece_length(piece)
    #レーンを考えていないのがよくない
    len = piece['length']
    if len.nil?
      len = piece['angle'].abs * piece['radius'] * Math.atan(1.0)*4.0 / 180.0
    end
    return len
  end

  def fun_p(x)
    if x <= 0.0
      1.0
    else
      0.5 ** x
    end
  end
  
  def AI(my_data)
    @piece_index = my_data['piecePosition']['pieceIndex']
    piece = @initial_data['race']['track']['pieces'][@piece_index]
    
    @lap = my_data['piecePosition']['lap']
    prev_dist = @dist
    @dist = @lap * @acc_dist[-1] + @acc_dist[@piece_index] -
      get_piece_length(@initial_data['race']['track']['pieces'][@piece_index]) +
      my_data['piecePosition']['inPieceDistance']
    @speed = @dist - prev_dist
    @time = @time + 1
    d_car_angle = my_data['angle'] - (if @car_angle.nil? then 0 else @car_angle end)
    @car_angle = my_data['angle']
    @log_list.add_log(RunLog.new(@time, @dist, @speed, @car_angle, my_data['piecePosition']['lane'], @initial_data['race']['track']['pieces'][@piece_index]))
    
    if piece['angle'] == nil
      return throttle_message(0.6)
    else
      return throttle_message(fun_p(d_car_angle) * ((58.0 - @car_angle.abs)/58.0) ** 2)
    end
  end
end

NoobBot.new(server_host, server_port, bot_name, bot_key)
