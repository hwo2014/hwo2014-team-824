import java.util.ArrayList;

JSONObject raw, raceInfo;
JSONArray tickInfo, pieceInfo;

int graph_height  = 200;
int start_time = 0, view_time = 0;
final int max_time = 1000;

int visnum = 0;
color black;

class VisParam {
  FloatList values;
  String name;
  boolean draw_p;
  int idx;

  float x, y;
  color c;

  VisParam(String name) {
    this.values = new FloatList();
    this.name   = name;
    this.idx    = visnum++;
    this.draw_p = false;

    c = color(45*idx%360, 70, 80);
    x = 10;
    y = 10+20*idx;
  }

  void clicked(float mx, float my) {
    if (mx > x && mx < x+10 && my > y && my < y+10) {
      draw_p = !draw_p;
    }
  }

  void push(float value) {
    values.append(value);
  }

  float get(int idx) {
    return values.get(idx);
  }

  void draw() {
    noStroke();
    fill(draw_p?c:color(0));
    rect(10,10+20*idx,10,10);
    fill(0);
    text(name, 30, y+10);

    if (!draw_p) return;

    pushMatrix();
    translate(150, graph_height+10);

    float mx = max(values.max(), -values.min());
    noFill();
    stroke(c);
    beginShape();
    for (int t=start_time; t<min(values.size(), start_time+max_time); t++) {
      vertex(t-start_time, map(values.get(t), -mx, mx, graph_height, -graph_height));
    }
    endShape();

    popMatrix();
  }
}

IntList time    = new IntList();
IntList piece   = new IntList();

VisParam speed   = new VisParam("speed");
VisParam dspeed  = new VisParam("dspeed");
VisParam angle   = new VisParam("angle");
VisParam dangle  = new VisParam("dangle");
VisParam ddangle = new VisParam("ddangle");
VisParam inertia = new VisParam("inertia");
VisParam estimated_ddangle = new VisParam("estimated_ddangle");
VisParam diff = new VisParam("diff");
VisParam[] vps = {speed, dspeed, angle, dangle, ddangle, inertia, estimated_ddangle, diff};

float getRadius(int pieceIdx) {
  return pieceInfo.getJSONObject(pieceIdx).getFloat("radius", -1);
}

boolean isCurve(int pieceIdx) {
  return getRadius(pieceIdx) > 0;
}

void setup() {
  colorMode(HSB, 360, 100, 100);
  size(max_time+170, graph_height*2+20);
  // initialize color
  colorMode(HSB, 360, 100, 100);
  black = color(0);

  // initialize data
  raw = loadJSONObject("runlog.json");
  raceInfo = raw.getJSONObject("race");
  tickInfo = raw.getJSONArray("log");
  pieceInfo = raceInfo.getJSONObject("track").getJSONArray("pieces");

  float anp = 0, dp = 0, sp = 0;
  float friction = 0.35626352;
  boolean moving = false;

  for (int i = 0; i < tickInfo.size(); i++) {
    JSONObject run = tickInfo.getJSONObject(i);

    int   t = run.getInt("gameTick");
    int   p = run.getInt("piece");
    float s = run.getFloat("speed");
    float an = run.getFloat("angle");
    float th = run.getFloat("throttle");
    float dan = (i<=5)?0:(an-anp);
    float ac = (i<=5)?0:(s-sp);
    float r = getRadius(p);
    float hoge = 1;
    float in = -ac*sin(radians(an));

    if (r>0)
      in += s*s*cos(radians(an))/r;

    // if (moving && abs(dan) < 0.01)
    //   moving = false;
    // if (abs(in) > friction)
    //   moving = true;

    // if (moving) {
    //   if (dan < 0)
    //     edda += friction;
    //   else
    //     edda -= friction;
    // } else {
    //   edda = 0;
    // }
    float edda = (i<=2)?0:(-angle.get(i-2)*0.0117389-dangle.get(i-1)*0.111724);

    speed.push(s);
    dspeed.push(ac);
    angle.push(an);
    dangle.push(dan);
    ddangle.push(dan-dp);
    inertia.push(in);
    estimated_ddangle.push(edda);
    diff.push(edda-dan+dp);

    piece.append(p);

    anp = an;
    dp = dan;
    sp = s;
  }

  for(int i=490; i<=540; i++) {
    System.out.printf("%d, %f, %f, %f, %f\n", i, angle.get(i), dangle.get(i+1), ddangle.get(i+2), estimated_ddangle.get(i));
  }
}

void mousePressed() {
  for(VisParam vp : vps) {
    vp.clicked(mouseX, mouseY);
  }
}

void draw() {
  background(0, 0, 100);

  pushMatrix();
  translate(150, graph_height+10);
  stroke(0);
  line(-10, 0, max_time+10, 0);
  colorMode(HSB, 360, 100, 100);
  for (int i = start_time; i<min(start_time+max_time, piece.size()); i++) {
    if (i % 20 == 0) {
      stroke(240);
      line(i-start_time, -graph_height, i-start_time, graph_height);
    }

    if (isCurve(piece.get(i)))
      stroke(color(piece.get(i)*37%360, 80, 30));
    else
      stroke(color(piece.get(i)*37%360, 25, 90));
    line(i-start_time, 10, i-start_time, 15);
  }
  stroke(0);
  line(view_time-start_time, -graph_height, view_time-start_time, graph_height);
  text(""+view_time, view_time-start_time, -20);
  popMatrix();

  for (VisParam vp : vps) {
    vp.draw();
  }
}

void mouseWheel(MouseEvent event) {
  float e = event.getAmount();
  start_time = max(0, start_time+25*(int)e);
}

void mouseMoved() {
  view_time = start_time+constrain(mouseX, 150, 150+max_time)-150;
}
